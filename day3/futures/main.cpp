#include <iostream>
#include <future>
#include <thread>
#include <stdexcept>

using namespace std;

int answer(int id)
{
    this_thread::sleep_for(4s);
    return 42;
}

int may_throw(int arg)
{
    if (arg == 13)
        throw runtime_error("Error");
    return arg*2;
}

int main()
{
    cout << "Hello futures!" << endl;

    //future<int> result = async(launch::async, answer, 10);

    packaged_task<int(int)> task(answer);
    future<int> result = task.get_future();
    auto l = [task = move(task)] () mutable { task(10); };
    thread th1(move(l));
    th1.detach();

    future<int> result1 = async(launch::async, may_throw, 10);
    future<int> result2 = async(launch::async, may_throw, 13);

    result1.wait();
    result2.wait();

    cout << "After launching" << endl;
    try
    {
        cout << "Answer1 = " << result1.get() << endl;
        cout << "Answer2 = " << result2.get() << endl;
    }
    catch(const runtime_error& err)
    {
        cout << err.what() << endl;
    }

    return 0;
}

