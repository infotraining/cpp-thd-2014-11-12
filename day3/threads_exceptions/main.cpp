#include <iostream>
#include <thread>
#include <stdexcept>
#include "../../utils.h"

using namespace std;

void may_throw(int arg)
{
    if (arg == 13)
        throw runtime_error("Error");
    cout << "Just got" << arg << endl;
}

void worker(exception_ptr& exc)
{
    try
    {
        for (int i = 0 ; i < 15 ; ++i)
        {
            may_throw(i);
        }
    }
    catch(...)
    {
        exc = current_exception();
    }
}
int main()
{
    cout << "Hello World!" << endl;
    exception_ptr ptr;
    scoped_thread th1(worker, ref(ptr));
    th1.join();
    if(ptr)
    {
        try
        {
            rethrow_exception(ptr);
        }
        catch(...)
        {
            cout << "Got exception " << endl;
        }
    }
    return 0;
}

