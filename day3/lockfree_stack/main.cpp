#include <iostream>
#include <stdexcept>
#include <mutex>
#include <atomic>
#include "../../utils.h"

using namespace std;

mutex cmutex;

template<typename T>
struct node
{
    node* prev;
    T data;
};

template<typename T>
class Stack
{
    atomic<node<T>*> head;
    //mutex mtx;
public:
    Stack() : head(nullptr) {}
    void push(T item)
    {
        node<T>* new_node = new node<T>;
        new_node->data = item;
        {
            new_node->prev = head.load();
            while(!head.compare_exchange_weak(new_node->prev, new_node));
            /*
             new_node->prev = head;
             head = new_node;
            */
        }
    }

    T pop()
    {
        node<T>* old_node;

        old_node = head.load();
        while(!head.compare_exchange_weak(old_node, old_node->prev));

        T res = old_node->data;
        delete old_node;
        return res;
    }
};

void push_and_pop(Stack<int>& s, int start)
{
    for (int i = start ; i < start + 1000 ; ++i)
        s.push(i);

    for (int i = start ; i < start + 1000 ; ++i)
    {
        lock_guard<mutex> l(cmutex);
        cout << s.pop() << " ; ";
    }
}

int main()
{
    Stack<int> s;
    scoped_thread th1(push_and_pop, ref(s), 0);
    scoped_thread th2(push_and_pop, ref(s), 1000);
    return 0;
}


