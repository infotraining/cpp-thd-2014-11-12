#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H

#include <thread>
#include "../../day2/TSQ/thread_safe_queue.h"
#include <vector>
#include <functional>

using task_t = std::function<void()>;

class active_object
{
    thread_safe_queue<task_t> q;
    std::thread servant;

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if (!task)
                return;
            task();
        }
    }

public:
    active_object() : q(2048)
    {
        servant = std::move(std::thread(&active_object::worker, this));
    }

    void send(task_t task)
    {
        q.push(task);
    }

    ~active_object()
    {
        q.push(nullptr);
        servant.join();
    }
};

#endif // ACTIVE_OBJECT_H
