#include <iostream>
#include "thread_pool.h"

using namespace std;

void myfunction()
{
    cout << "My function is called from " << this_thread::get_id()  << endl;
}

void myfunction2(int id)
{
    cout << "My function " << id << " is called from " << this_thread::get_id()  << endl;
}

int answer()
{
    return 42;
}

int main()
{

    thread_pool tp(4);
    tp.submit(myfunction);
    tp.submit( [] { cout << "My lambda from " << this_thread::get_id()  << endl;});
    int number = 100;
    tp.submit( [number] { myfunction2(number); });

    for (int i = 0 ; i < 20; ++i)
    {
        tp.submit( [i] { cout << "Tasl " << i << " from " << this_thread::get_id() << endl;
                        this_thread::sleep_for(chrono::seconds(1)); });
    }
    future<int> result = tp.async(answer);
    cout << "Waiting for results..." << endl;
    cout << "Answer " << result.get() << endl;
    return 0;
}

