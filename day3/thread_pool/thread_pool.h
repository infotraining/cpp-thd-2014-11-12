#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <functional>
#include <vector>
#include <thread>
#include <future>
#include "../../day2/TSQ/thread_safe_queue.h"
#include "../../utils.h"

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q;
    std::vector<scoped_thread> pool;

public:
    thread_pool(int pool_size) : q(100)
    {
        for (int i = 0 ; i < pool_size ; ++i)
        {
            pool.emplace_back(&thread_pool::worker, this);
        }
    }

    void submit(task_t task)
    {
        if(task)
            q.push(task);
    }

    template<typename F>
    auto async(F&& fun) -> std::future<decltype(fun())>
    {
        using Res = decltype(fun());

        // using shared ptr because std::function doesn't support moving
        auto task = std::make_shared<std::packaged_task<Res()>>(std::move(fun));
        std::future<Res> fres = task->get_future();
        q.push([task] { (*task)(); } );
        return fres;
    }

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if (!task) return;
            task();
        }
    }

    ~thread_pool()
    {
        for (auto& th : pool)  q.push(nullptr);
    }
};

#endif // THREAD_POOL_H
