#include <iostream>
#include <future>
#include <thread>
#include <stdexcept>
#include <numeric>
#include <algorithm>
#include <vector>
#include "../../utils.h"

using namespace std;

template<typename It, typename T>
T parallel_accumulate(It begin, It end, T init)
{
    // divide
    int N = max(thread::hardware_concurrency(), (unsigned int)1);
    vector<future<T>> results;
    size_t block_size = distance(begin, end)/N;
    It block_start = begin;

    // launch async
    for (int i = 0 ; i < N ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if (i == N-1) block_end = end;
        results.push_back( async (launch::async,
                                  accumulate<It, T>,
                                  block_start, block_end, T()));
        block_start = block_end;
    }

    // sum futures
    return accumulate(results.begin(), results.end(), init,
                      [] (T x, future<T>& y) { return x + y.get();});
}

int main()
{
    const size_t SIZE = 1000000000;
    cout << "Hello World!" << endl;
    vector<long> v;
    {
        Timer<chrono::milliseconds> t("creating");
        for (long i = 0 ; i < SIZE ; ++i)
        {
            v.push_back(i);
        }
    }
    {
        Timer<chrono::milliseconds> t("accumulate");
        cout << accumulate(v.begin(), v.end(), 0L) << endl;
    }


    {
        Timer<chrono::milliseconds> t("parallel accumulate");
        cout << parallel_accumulate(v.begin(), v.end(), 0L) << endl;
    }
    return 0;
}

