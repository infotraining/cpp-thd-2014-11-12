#include <iostream>
#include <thread>
#include <mutex>
#include "../../utils.h"

using namespace std;

timed_mutex mtx;
MyData mydata;

//void work_with_data(std::function<void(MyData& data)> operand)
//{
//    unique_lock<timed_mutex> lk(mtx);
//    operand(my_data);
//}

void worker()
{
    cout << "Worker has started " << this_thread::get_id() << endl;
    unique_lock<timed_mutex> lk(mtx, try_to_lock);
    if (!lk.owns_lock())
    {
        do
        {
            cout << "THD# " << this_thread::get_id() << " doesn't own the lock" << endl;
        }
        while(!lk.try_lock_for(chrono::milliseconds(100)));
    }
    cout << this_thread::get_id() << " got lock" << endl;
    this_thread::sleep_for(1s);
    cout << "Finishing..." << this_thread::get_id() << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    scoped_thread th1(worker);
    scoped_thread th2(worker);
    return 0;
}


