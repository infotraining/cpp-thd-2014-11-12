#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include "../../utils.h"
#include <atomic>
#include <condition_variable>
#include <mutex>

using namespace std;

namespace atomic_solution
{

class Data
{
    vector<int> data_;
    //volatile bool is_ready;
    atomic<bool> is_ready;

public:
    Data() : is_ready(false)
    {}

    void read()
    {
        cout << "Reading data.." << endl;
        this_thread::sleep_for(chrono::seconds(100));
        data_.resize(100);
        generate(data_.begin(), data_.end(), [] {return rand() % 100;});
        is_ready.store(true, memory_order_seq_cst);
        cout << "end reading" << endl;
    }

    void process()
    {
        for(;;)
        {
            if (is_ready)
            {
                cout << "processing..." << endl;
                long sum = accumulate(data_.begin(), data_.end(), 0L);
                cout << "sum = " << sum << endl;
                return;
            }
            this_thread::sleep_for(10us);
        }
    }
};

}

namespace conditional_solution
{

class Data
{
    vector<int> data_;
    bool is_ready;
    mutex mtx;
    condition_variable cond;

public:
    Data() : is_ready(false)
    {}

    void read()
    {
        cout << "Reading data.." << endl;
        this_thread::sleep_for(chrono::seconds(1));
        data_.resize(100);
        generate(data_.begin(), data_.end(), [] {return rand() % 100;});
        {
            lock_guard<mutex> lk(mtx);
            is_ready = true;
        }
        cond.notify_one();
        cout << "end reading" << endl;
    }

    void process()
    {
        unique_lock<mutex> lk(mtx);
//        while(!is_ready)
//        {
//            cout << "Waiting" << endl;
//            cond.wait(lk);
//        }
        cond.wait(lk, [this]() {return is_ready;});
        cout << "processing..." << endl;
        long sum = accumulate(data_.begin(), data_.end(), 0L);
        cout << "sum = " << sum << endl;
        return;

    }
};

}

using namespace conditional_solution;

int main()
{
    Data data;
    cout << "Hello World!" << endl;
    scoped_thread th1( [&data] { data.read();} );
    scoped_thread th2( [&data] { data.process();} );
    return 0;
}

