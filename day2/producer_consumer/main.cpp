#include <iostream>
#include <queue>
#include <thread>
#include <condition_variable>
#include <mutex>
#include "../../utils.h"

using namespace std;

queue<int> q;
condition_variable cond;
mutex mtx;


void producer()
{
    for (int i = 0 ; i < 1000 ; ++i)
    {
        {
            lock_guard<mutex> lg(mtx);
            q.push(i);
        }
        cond.notify_one();
        this_thread::sleep_for(20ms);
    }
}

void consumer()
{
    for(;;)
    {
        {
            unique_lock<mutex> lk(mtx);
            cond.wait(lk, [] () { return !q.empty();} );

            cout << this_thread::get_id() << " just got " << q.front() << endl;
            q.pop();
        }
        //this_thread::sleep_for(200ms);
    }
}

int main()
{
    cout << "Hello World!" << endl;
    scoped_thread th1(producer);
    scoped_thread th2(consumer);
    scoped_thread th3(consumer);
    return 0;
}

