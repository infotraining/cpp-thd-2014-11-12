#include <iostream>
#include "../../utils.h"
#include <mutex>
#include <atomic>

using namespace std;

const int n_of_iter = 1000000;
long counter = 0;
atomic<long> counter_atomic{0};
mutex mtx;

class spinlock_mutex
{
    std::atomic_flag flag;
public:
    spinlock_mutex() : flag(ATOMIC_FLAG_INIT) {}
    void lock()
    {
        while(flag.test_and_set());
    }

    void unlock()
    {
        flag.clear();
    }
};

spinlock_mutex spin_mtx;

void increase_spin()
{
    for (int i = 0 ; i < n_of_iter ; ++i)
    {
        lock_guard<spinlock_mutex> guard(spin_mtx);
        ++counter;
    }
}

void increase()
{
    for (int i = 0 ; i < n_of_iter ; ++i)
    {
        lock_guard<mutex> guard(mtx);
        //if (counter == 1000) return;
        ++counter;
    }
}

void increase_atomic()
{
    for (int i = 0 ; i < n_of_iter ; ++i)
    {
        counter_atomic.fetch_add(1);
    }
}


void increase_no_mutex()
{
    for (int i = 0 ; i < n_of_iter ; ++i)
    {
        ++counter;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    {
        Timer<std::chrono::milliseconds> timer1("mutex");
        scoped_thread t1(increase);
        scoped_thread t2(increase);
    }
    cout << "Counter with mutex = " << counter << endl;
    counter = 0;
    {
        Timer<std::chrono::milliseconds> timer1("spinlock");
        scoped_thread t1(increase_spin);
        scoped_thread t2(increase_spin);
    }
    cout << "Counter with mutex = " << counter << endl;
    counter = 0;
    {
        Timer<std::chrono::milliseconds> timer1("no mutex");
        scoped_thread t1(increase_no_mutex);
        scoped_thread t2(increase_no_mutex);
    }
    cout << "Counter = " << counter << endl;
    {
        Timer<std::chrono::milliseconds> timer1("atomic");
        scoped_thread t1(increase_atomic);
        scoped_thread t2(increase_atomic);
    }
    cout << "is atomic?" << counter_atomic.is_lock_free() << endl;
    cout << "Counter atomic  = " << counter_atomic << endl;
    return 0;
}

