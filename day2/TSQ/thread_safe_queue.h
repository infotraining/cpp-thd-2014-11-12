#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H
#include <mutex>
#include <condition_variable>
#include <queue>

template<typename T>
class thread_safe_queue
{
    std::queue<T> q;
    std::condition_variable cond;
    std::condition_variable in_cond;
    std::mutex mtx;
    size_t limit;
public:
    thread_safe_queue(size_t limit) : limit(limit) {}
    void push(T item)
    {
        std::unique_lock<std::mutex> lg(mtx);
        while(q.size() >= limit)
        {
            in_cond.wait(lg);
        }
        q.push(item);
        cond.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lk(mtx);
        cond.wait(lk, [this] () { return !q.empty();} );
        item = q.front();
        q.pop();
        in_cond.notify_one();
    }

    bool try_pop(T& item)
    {
        std::unique_lock<std::mutex> lk(mtx);
        if (q.empty()) return false;
        item = q.front();
        q.pop();
        in_cond.notify_one();
        return true;
    }
};

#endif // THREAD_SAFE_QUEUE_H
