#include <iostream>
#include <queue>
#include <thread>
#include <condition_variable>
#include <mutex>
#include "../../utils.h"
#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<long> q(100   );
condition_variable cond;
mutex mtx;


void producer()
{
    for (int i = 0 ; i < 100000000 ; ++i)
    {
        q.push(i);
        //this_thread::sleep_for(20ms);
    }
}

void consumer()
{
    for(;;)
    {
        long item;
        q.pop(item);
        cout << this_thread::get_id() << " just got " << item << endl;

        this_thread::sleep_for(200ms);
    }
}

int main()
{
    cout << "Hello World!" << endl;
    scoped_thread th1(producer);
    scoped_thread th2(consumer);
    scoped_thread th3(consumer);
    return 0;
}

