#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread " << this_thread::get_id() << endl;
}

void hello_id(int id)
{
    cout << "Hello from thread " << id << endl;
}

class ThreadGuard
{
    thread& th_;
public:
    ThreadGuard(thread& th) : th_(th) {}
    ThreadGuard(const ThreadGuard&) = delete;
    ThreadGuard& operator=(const ThreadGuard&) = delete;
    ~ThreadGuard()
    {
        if (th_.joinable())
            th_.join();
    }
};

class scoped_thread
{
    thread th_;
public:
    scoped_thread(thread&& th) : th_(move(th)) {}

    template<typename... T>
    scoped_thread(T&&... args) : th_(forward<T>(args)...) {}

    // copy constr + copy operator = deleted
    scoped_thread(const scoped_thread&) = delete;
    scoped_thread& operator=(const scoped_thread&) = delete;
    // move constr + move operator = on
    scoped_thread(scoped_thread&&) = default;
    scoped_thread& operator=(scoped_thread&&) = default;

    ~scoped_thread()
    {
        if (th_.joinable())
            th_.join();
    }
};

scoped_thread make_thread()
{
    return scoped_thread(thread(hello_id, 100));
}

int main()
{
    cout << "Hello World!" << endl;
    thread th1(&hello_id, 10);
    thread th2(&hello_id, 20);

    ThreadGuard g1(th1);
    ThreadGuard g2(th2);

    thread th3(&hello_id, 40);
    scoped_thread g3(thread(&hello_id, 30));
    scoped_thread g4(move(th3));

    scoped_thread g5(&hello_id, 200);

    vector<scoped_thread> threads;
    threads.emplace_back(&hello_id, 300);
    scoped_thread g(make_thread());
    return 0;
}
