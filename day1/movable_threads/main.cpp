#include <iostream>
#include <thread>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread " << this_thread::get_id() << endl;
}

void hello_id(int id)
{
    cout << "Hello from thread " << id << endl;
}

void work_on_data(int& data)
{
    this_thread::sleep_for(chrono::seconds(1));
    cout << "DAta: " << data << endl;
}

thread make_thread(int id)
{
    thread tmp(work_on_data, ref(id));
    return tmp;
}

int main()
{
    vector<thread> threads;
    // option 1 : forced move
    thread tmp(hello);
    threads.push_back(move(tmp));

    // option 2 : temp (rvalue)
    threads.push_back(thread(&hello));

    // option 3 : emplace
    threads.emplace_back(&hello_id, 10);
    threads.push_back( make_thread(100));

    for( thread& th : threads)
        th.join();
    return 0;
}

