#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread " << this_thread::get_id() << endl;
}

void hello_id(int id)
{
    cout << "Hello from thread " << id << endl;
}

void answer(int& result)
{
    result = 42;
}

class BackgroundTask
{
    const int id_;
public:
    BackgroundTask(int id) : id_(id) {}
    void operator ()(int delay)
    {
        for (int i = 0 ; i < 10 ; ++i)
        {
            cout << "BT #" << id_ << ": " << i << endl;
            this_thread::sleep_for(chrono::milliseconds(delay));
        }
    }
};

class Buff
{
    vector<int> buff_;
public:
    void assign(const vector<int>& src)
    {
        buff_.assign(src.begin(), src.end());
    }

    vector<int> data() const
    {
        return buff_;
    }
};

int main()
{
    cout << "Hello Threads!" << endl;
    thread th1(&hello);
    thread th2(&hello_id, 10);
    int res = 0;
    //thread th3( bind(answer, res));
    thread th3( &answer, ref(res));

    BackgroundTask bt(20);
    thread th4(bt , 100);
    vector<int> vec = {1,2,3,4,5,6};
    Buff buff1;
    Buff buff2;
    Buff buff3;


    thread th6( bind( &Buff::assign, ref(buff2), cref(vec)));

    thread th7( [] (int id) { cout << "Lambda " << id << " is called" << endl; }, 30 );

    thread th5( &Buff::assign, &buff1, cref(vec));
    thread th8( [&] { buff3.assign(vec); } );

    cout << "After launching the thread" << endl;
    th1.join();
    th2.join();
    th3.join();
    cout << "Result = " << res << endl;
    th4.join();
    th5.join();
    th6.join();
    th7.join();
    for (int el : buff1.data())
        cout << el << ": ";
    cout << endl;
    return 0;
}

