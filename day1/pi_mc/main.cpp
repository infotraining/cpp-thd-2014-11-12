#include <random>
#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <numeric>
#include "../../utils.h"
#include <mutex>

using namespace std;
mutex mtx;

void calc_pi(long N, long& counter)
{
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    double x, y;

    for (int n = 0; n < N; ++n) {
        x = dis(gen);
        y = dis(gen);
        if( x*x + y*y < 1)
        {
            mtx.lock();
            ++counter;
            mtx.unlock();
        }
    }
}

int main()
{
    const long N = 10000000;
    long counter = 0;
    {
        Timer<std::chrono::milliseconds> t("No threads");
        calc_pi(N, counter);
    }
    cout << "Pi = " << double(counter)/double(N)*4 << endl;

    //threads
    int n = thread::hardware_concurrency();
    n = 4;
    //vector<long> counters(n);
    counter = 0 ;
    {
        Timer<std::chrono::milliseconds> t("Threads");
        vector<scoped_thread> thds;
        for (int i = 0 ; i < n ; ++i)
            thds.emplace_back(calc_pi, N/n, ref(counter));

        //for (thread& th :thds) th.join();
    }
    cout << "Pi = ";
    //cout << double(std::accumulate(counters.begin(), counters.end(), 0L))/double(N)*4 << endl;
    cout << "Pi = " << double(counter)/double(N)*4 << endl;

}
